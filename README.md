
[[_TOC_]]
# Guide des bonnes pratiques GitLab Issues
## Glossaire
**Etiquette** ou **Label** : Une étiquette est un élément qui permet de préciser un attribut ou une catégorie d'un ticket. Elles apparaissent sous la forme de mots-clefs sur fond coloré. Il est possible d'assigner plusieurs étiquettes à un ticket. Il est également possible de créer et personnaliser des étiquettes. Les étiquettes permettent de faciliter les recherches et filtrages.

**Groupe** : Un groupe GitLab est un ensemble de projets ayant les mêmes utilisateurs. Il peut également contenir des sous-groupes et ainsi former une arborescence. Un utilisateur n'ayant pas accès à un groupe ne peut pas avoir accès aux projets qu'il contient.

**Tâches** ou **Tasks** : Une tâche est une étape pour résoudre un ticket. Une tâche peut avoir un statut ouvert ou fermé, une personne assignée, des étiquettes, une date de début et d'échéance, ainsi qu'une description.

## Prise en main rapide
### Comment changer la langue
>Pour **changer la langue**, il faut :
>- cliquer sur l'icône profil en haut à droite
>- sélectionner "Préférences" (ou "Preferences" si l'application est en anglais)\
>![profil](Images/accesProfil.png){height=380px}
>- dans la partie "Localisation" (ou "Localization"), modifier le champ "Langue" (ou "Language")
>- sauvegarder avec le bouton "Save changes"\
>![langue](Images/changerLangue.png){height=380px}

La suite de ce document se base sur la version française de GitLab.

### Comment changer les paramètres de notifications
Il existe 4 niveaux de notification :
>- **Watch** : Vous recevrez une notification par mail pour toute nouvelle activité, sur tous vos projets ou sur tous les projets du groupe concerné.
>- **Participate** : Vous recevrez une notification par mail pour toute nouvelle activité sur un ticket auquel vous avez participé.
>- **On mention** : Vous recevrez une notification par mail quand un autre utilisateur vous mentionne dans un commentaire à l'aide de `@votreNomDUtilisateur`.
>- **Disabled** : Vous ne recevrez aucune notification.

Pour **changer les paramètres de notifications**, il faut :
>- cliquer sur l'icône profil en haut à droite
>- sélectionner "Modifier le profil"\
>![profil](Images/accesProfil.png){height=380px}
>- dans le menu de gauche, cliquer sur "Notifications"
Vous pouvez ensuite :
>>- modifier le niveau de notification global, ce qui changera le niveau de notification pour l'ensemble de vos projets\
>>![notificationGlobale](Images/notificationGlobale.png){height=380px}
>>- modifier le niveau de notification pour un *groupe GitLab* en particulier *(voir glossaire)*. Si le niveau de notification global et le niveau de notification d'un groupe dont différents, c'est le niveau de notification du groupe qui sera pris en compte.\
>>![notificationDeGroupe](Images/notificationGroupe.png){height=380px}

### Comment afficher les tickets
En vous connectant sur GitLab, vous arriverez sur une page avec tous les projets auxquels vous participez. Il faut donc cliquer sur le projet dont vous voulez afficher les tickets.

Pour l'**affichage classique des tickets**, il faut : 
>- dans le menu de gauche, passer la souris sur "Tickets"
>- dans le sous menu qui s'affiche, cliquer sur "Liste"\
![afficherTickets](Images/afficherTickets.png)\{height=380px}
![affichageTickets](Images/affichageTickets.png){height=380px}
La page affiche par défaut uniquement les tickets ouverts, il est possible d'accéder aux tickets fermés  via l'onglet "Closed" ou à tous les tickets via l'onglet "All".\
![ouvertFermé](Images/ouvertFerme.png){height=380px}

### Recherche de tickets
Il est possible de rechercher des tickets parmi tous les projets en utilisant le champ "Rechercher sur GitLab" situé en haut de la page, à côté du logo GitLab.

Pour chercher des tickets au sein d'un projet, il faut se rendre sur la liste de ses tickets.
En haut de la liste des tickets il y a un champ de recherche.

Les critères de recherche les plus utiles sont :
>- le numéro d'id du ticket
>- **Assignee** : la personne assignée au ticket
>- **Author** : le rapporteur du ticket
>- **Confidential** : le niveau de confidentialité du ticket
>- **Label** : une étiquette du ticket(y compris "priorité", "impact" ou "version")
>- **Search within** : mots contenus dans le titre ou la description du ticket
>![criteresRecherche](Images/rechercheLabel1.png){height=380px}

Pour **faire une recherche par l'id du ticket**, il ne faut pas oublier le "#" devant le numéro d'id.\
![rechercheParId](Images/rechercheID.png){height=380px}


Pour **faire une recherche par étiquette** il faut : 
>- cliquer dans le champ de recherche
>- dans le sous-menu, cliquer sur "label"\
>![rechercheLabel](Images/rechercheLabel1.png){height=380px}
>- dans le sous-menu, cliquer sur l'opérateur logique de votre choix ("is" dans notre cas). Les opérateurs logiques peuvent être :
>>- **is (=)** : les tickets qui possèdent cette étiquette
>>- **is not one of (!=)** : les tickets qui ne possèdent pas cette étiquette
>>- **is one of (||)** : les tickets qui possèdent au moins une des étiquettes avec le critère de recherche "is one of" (nécessite plusieurs critères de recherche utilisant cet opérateur)\
>![rechercheLabelOpérateur](Images/rechercheLabel2.png){height=380px}
>- dans le sous-menu, sélectionner l'étiquette de votre choix\
>![rechercheLabelEtiquette](Images/rechercheLabel3.png){height=380px}
![resultatRechercheLabel](Images/resultatRechercheLabel.png){height=380px}

Pour **faire une recherche dans la description** il faut :
>- cliquer dans le champ de recherche
>- dans le sous-menu, cliquer sur "Search within"\
>![searchWithin](Images/chercherDans.png){height=380px}
>- dans le sous-menu, cliquer sur "Descriptions". Il est aussi possible de faire une recherche dans les titres en cliquant sur "Titles".\
>![description](Images/description.png){height=380px}
>- saisir les mots qu'on souhaite chercher\
>![rechercheDescription](Images/chercherDescription.png){height=380px}

Il est possible de **trier les tickets** selon plusieurs critères. Les plus utiles sont :
>- **Created date** : date de création du ticket
>- **Updated date** : date de dernière modification du ticket
>- **Closed date** : date de fermeture du ticket
>- **Title** : titre dans l'ordre alphabétique
![tri](Images/tri.png){height=380px}
Pour passer d'un tri croissant à un tri décroissant (ou inversement), il faut cliquer sur le bouton situé à côté du type de tri.\
![sensTri](Images/sensTri.png){height=380px}

### Consulter un ticket

Une fois sur la page d'un ticket, on peut trouver ces informations :
>1. Le titre du ticket
>2. Le statut du ticket (ouvert/fermé)
>3. La date de création du ticket
>4. Le rapporteur du ticket
>5. La description du ticket
>6. **Tasks** : les tâches 
>7. **Linked items** : les liens avec d'autres tickets
>8. L'icône flèche en haut à droite permet d'afficher ou cacher l'encadré de droite\
>![ticketPart1](Images/ticket1.png){height=380px}
>9. Dans l'encadré de droite, les informations les plus utiles sont : 
>>- **Assignees** : personnes assignées au ticket
>>- **Labels** : étiquettes du ticket
>>- **Confidentiality** : le niveau de confidentialité du ticket
>10. **Activity** : les modifications et commentaires du ticket, ainsi que le champ de saisie de commentaire\
>![ticketPart2](Images/ticket2.png){height=380px}

### Comment écrire un commentaire

Pour **écrire un commentaire** sur un ticket, il faut :
>- se rendre sur la page du ticket
>- descendre jusqu'à la section "Activity"
>- saisir son commentaire dans le champ de la section "Activity"
>![ecrireCommmentaire](Images/saisieCommentaire.png){height=380px}
>- il est également possible d'ajouter une image ou un document en cliquant sur l'icône trombone
>![pieceJointeCommentaire](Images/pieceJointeCommentaire.png){height=380px}
>- cliquer sur le bouton "Comment"

### Déplacer des tickets vers un autre projet

Pour **déplacer des tickets vers un autre projet**, il faut :
>- sur la page liste des tickets, cliquer sur le bouton "Edit issues"\
>![modifierTickets](Images/editIssue.png){height=380px}
>- Des cases à cocher apparaissent à côté des titres des tickets, il faut cocher les tickets à déplacer
>- dans l'encadré de droite, cliquer sur le bouton "Move selected"\
>![deplacerTickets](Images/deplacerIssue.png){height=380px}
>- sélectionner le projet de destination
>- cliquer sur le bouton "Move"


**{- Point d'attention : conséquences du déplacement de tickets -}**
- {- Le ticket sera conservé et fermé dans le projet d'origine. -}
- {- L'id du ticket sera différent dans le projet de destination. -}
- {- Si le projet de destination ne possède pas une des étiquettes du ticket d'origine, cette étiquette n'existera pas sur le ticket déplacé. -}



## Bonnes pratiques

### Création d'un ticket
Pour **créer un nouveau ticket**, il faut cliquer sur le bouton "New issue" sur la page de liste des tickets.
![nouveauTicket](Images/newIssue.png){height=380px}

L'utilisation d'*étiquettes* et une *description qui inclut les étapes pour reproduire l'anomalie* permettent une meilleur *clarté des tickets* et facilitent les *recherches et filtrages*. Il est possible de créer de nouvelles étiquettes.
Les seuls champs obligatoires sont le titre et le type, mais nous vous conseillons de renseigner au minimum ces champs :
>- Titre
>- Type
>- Description (en incluant les étapes pour reproduire)
>- Une étiquette pour chacune de ces catégories :
>>- *Anomalie* ou *Evolution*
>>- Impact (*Impact : Mineur* ou *Impact : Majeur* ou *Impact : Bloquant*)
>>- Priorité (*Priorité : Basse* ou *Priorité : Normale* ou *Priorité : Haute*)\
>>![selectionEtiquette](Images/selectLabel.png){height=380px}

Il y a deux options pour le **type de ticket** : *issue* qui correspond à une anomalie détectée en recette, et *incident* qui correspond à une anomalie détectée en production.


Pour **ajouter un document ou une image** à la description, il faut cliquer sur l'icône trombone de l'encadré description.\
![ajoutFichier](Images/ajoutFichier.png){height=380px}
![fichierAjouté](Images/fichierAjout%C3%A9.png){height=380px}

Il n'y a pas de champ "étapes pour reproduire l'anomalie" dans GitLab. Il faut donc ajouter ces information à la fin de la description.

Lorsque un ticket est créé il est automatiquement **ouvert**. Il faut le **fermer** lorsqu'il n'a plus besoin d'être ouvert. Pour fermer un ticket, il faut se rendre sur sa page et cliquer sur le bouton "Close issue" ou "Close incident".\
![fermerTicket](Images/fermerTicket.png){height=380px}

### Créer des liens entre les tickets

Pour signaler que deux tickets sont des doublons, sont dépendant entre eux ou sont liés, il faut créer un lien entre ces deux tickets. Ainsi, depuis la page d'un ticket on peut facilement retrouver les tickets qui lui sont liés.

Pour **créer un lien entre deux tickets**, il faut :
>- ouvrir la page d'un des tickets à lier
>- dans l'encadré "Linked items", cliquer sur le bouton "Add"\
>![ajouterLien](Images/links.png){height=380px}
>- saisir l'id de l'autre ticket à lier\
>![selectionnerLien](Images/linkIssue.png){height=380px}
>- cliquer sur le bouton "Add" bleu

### Utilisation des tableaux

Les tableaux sont un type d'affichage des tickets. On peut y accéder dans le menu de gauche > Tickets > Tableaux. Ils permettent une vue des tickets filtrés par statut(ouvert/fermé) ou par étiquette. Les tableaux sont communs à tous les participants au projet : si on modifie un tableau, il est modifié pour tous les autres utilisateurs également. Les tableaux sont mis à jour dynamiquement dès qu'un ticket est modifié ou créé. \
Dans le tableau par défaut, les tickets sont rangés dans deux listes : les tickets ouverts et les tickets fermés.\
![tableaux](Images/tableaux.png){height=380px}

Il est possible de créer de nouveaux tableaux avec d'autres types de listes, comme un tableau avec les différents niveaux de priorités par exemple.\
![tableauPersonnalisé](Images/tableauCustom.png){height=380px}

Pour **changer de tableau**, il faut cliquer sur le menu déroulant à gauche de la barre de recherche puis cliquer sur le titre du tableau.\
![changementTabeau](Images/changementTableau.png){height=380px}

Pour **créer un nouveau tableau personnalisé**, il faut :
>- dans le menu déroulant "Switch board" à gauche de la barre de recherche, cliquer sur "Create new board"\
>![menuSwitchBoard](Images/changerDeTableau.png){height=380px}
>- dans le pop-up "Create new board" :
>>- saisir un titre. C'est ce titre qu'on retrouvera dans le menu déroulant de sélection de tableau.
>>- décocher l'options "Show the Open list" si vous ne voulez pas afficher la liste des tickets ouverts et "Show the Closed list" si vous ne voulez pas afficher la liste des tickets fermés.
>- cliquer sur "Create board"\
>![creerTableau](Images/creerTableau.png){height=380px}
Le tableau créé est vide, il faut ensuite ajouter des listes.

Pour **ajouter une liste dans un tableau**, il faut :
>- cliquer sur le bouton "Create list" à droite de la barre de recherche\
>![creerListe](Images/creerListe.png){height=380px}
>- dans l'encadré "New list", sélectionner une étiquette dans le menu déroulant "Select label"\
>![selectionLabelListe](Images/editListe.png){height=380px}

Il est possible d'ordonner les listes avec un cliquer-glisser.\
![deplacerTableau](Images/glisserTableau.png){height=380px}

Il est également possible de changer un ticket de liste avec un cliquer-glisser. Le statut ou l'étiquette du ticket sont automatiquement modifiés pour correspondre à la liste dans laquelle il a été glissé.\
![changerDeListe](Images/changeLabelTableau.png){height=380px}

On peut faire une recherche dans un tableau, qui affichera les résultats triés dans les différentes listes. La recherche s'effectue de la même manière que dans la liste des tickets.\
![rechercheTableau](Images/rechercheTableau.png){height=380px}
![resultatRechercheTableau](Images/resultatRechercheTableau.png){height=380px}


Pour des renseignements plus détaillés, vous pouvez consulter la [documentation GitLab](https://docs.gitlab.com/ee/user/).


